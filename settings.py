import json, os, sys

class Settings:
    def __init__(self, fname):
        # set some default values
        self.app_port = 8300
        self.ner_port = 9000

        settings = json.load(open(fname, encoding = "utf8"))
        # update all properties from the settings file
        self.__dict__.update(settings)