import flask, uuid, time, json, io, os
from flask import request
from settings import Settings
from optparse import OptionParser
from ner_request import StanfordCoreNLP

app = flask.Flask(__name__)

global nlp
nlp = None

# settings loaded from the settings file
settings = None


@app.route("/ner", methods=["POST"])
def ner():
    obj = request.get_json()
    resObj = None
    if "sentence" in obj:
        resObj = extract_ner(obj["sentence"])
    return json.dumps(resObj)

def extract_ner(sentence):
    resObj = dict()
    resObj["sentence"] = sentence
    entity_mentions = nlp.ner(sentence)
    outNers = []
    for e in entity_mentions:
        for el in e:
            # temp_sen = temp_sen.replace(el.get('text'), el.get('ner'))
            outObj = {
                "ner": { "text": el.get('text'), "keyword": el.get('ner') } }
            outNers.append(outObj)
    resObj["ners"] = outNers
    return resObj

def test():
    extract_ner("Apple just bought startup XY Inc. for 9 billion")


# for debugging purposes, it's helpful to start the Flask testing
# server (don't use this for production

if __name__ == "__main__":
    parser = OptionParser(usage="Usage: %prog mode [options]")
    parser.add_option("-s", "--settings", dest="settingsFNm", action="store", type="string", default = "./settings.json", help = "Path to the settings file")

    options, args = parser.parse_args()

    assert os.path.exists(options.settingsFNm), "File %s does not exist. Unable to load the settings. Exiting" % (options.settingsFNm)

    # load settings
    settings = Settings(options.settingsFNm)

    print("* Starting web service...")

    nlp = StanfordCoreNLP('http://localhost', port=settings.ner_port)
    print("Connected to NLP server")

    test()

    app.run(port = settings.app_port)